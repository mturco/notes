package com.mturco.notes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DbHelper extends SQLiteOpenHelper {
	public final static String DATABASE_NAME = "notes.db";
	public final static int DATABASE_VERSION = 2;

	public final static String NOTES_TABLE = "notes";
	public final static String NOTES_ID_FIELD = "_id";
	public final static String NOTES_TITLE_FIELD = "title";
	public final static String NOTES_NOTE_FIELD = "note";
	public final static String NOTES_DATE_CREATED_FIELD = "date_created";
	public final static String NOTES_DATE_MODIFIED_FIELD = "date_modified";
	
	public final static String TAGS_TABLE = "tags";
	public final static String TAGS_ID_FIELD = "_id";
	public final static String TAGS_NOTE_ID_FIELD = "note_id";
	public final static String TAGS_TAG_FIELD = "tag";
	
	public DbHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("create table if not exists " + NOTES_TABLE + " (" +
				NOTES_ID_FIELD + " integer primary key autoincrement, " +
				NOTES_TITLE_FIELD + " text not null, " +
				NOTES_NOTE_FIELD + " text, " +
				NOTES_DATE_CREATED_FIELD + " integer, " +
				NOTES_DATE_MODIFIED_FIELD + " integer" +
				"); " +
				"create index " + NOTES_TABLE + "_" + NOTES_TITLE_FIELD + "_index on " + NOTES_TABLE + " (" + NOTES_TITLE_FIELD + ");"
				);
		
		db.execSQL("create table if not exists " + TAGS_TABLE + " (" +
				TAGS_ID_FIELD + " integer primary key autoincrement, " +
				TAGS_NOTE_ID_FIELD + " integer, " +
				TAGS_TAG_FIELD + " text not null" +
				"); " +
				"create index " + TAGS_TABLE + "_" + TAGS_NOTE_ID_FIELD + "_index on " + TAGS_TABLE + " (" + TAGS_NOTE_ID_FIELD + "); " +
				"create index " + TAGS_TABLE + "_" + TAGS_TAG_FIELD + "_index on " + TAGS_TABLE + " (" + TAGS_TAG_FIELD + ");"
				);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(getClass().getName(), "Upgrading database from version " + oldVersion +
				" to " + newVersion + ", which will destroy all current data.");
		db.execSQL("drop table if exists " + NOTES_TABLE);
		db.execSQL("drop table if exists " + TAGS_TABLE);
		onCreate(db);
	}
	
	public boolean importDatabase(String path) {
		try {
            File currentDb = new File("/data/data/com.mturco.notes/databases/" + DATABASE_NAME);
            File backupDb = new File(path);
            
            close();
            
            FileChannel source = new FileInputStream(backupDb).getChannel();
            FileChannel destination = new FileOutputStream(currentDb).getChannel();
            destination.transferFrom(source, 0, source.size());
            source.close();
            destination.close();
            
            getWritableDatabase().close();
            
            return true;
        }
        catch (Exception e) {
        	Log.e(getClass().getName(), "Exception: " + e.getMessage());
        	return false;
        }
	}
}