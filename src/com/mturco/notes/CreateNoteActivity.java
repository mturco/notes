package com.mturco.notes;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * Used to add new notes.
 * 
 * Note: This activity is nearly identical to EditNoteActivity (which is used for
 * editing existing notes). The only visual difference is the addition of the title
 * EditText.
 * 
 * @see EditNoteActivity
 */
public class CreateNoteActivity extends Activity {
	private static final int SET_TAGS = 1;
	private DbAdapter adapter;
	private long noteId = -1;
	private boolean discardNote = false;
	private boolean noteSaved = false;
	private EditText titleEditText;
	private EditText noteEditText;
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.create_note);
		getViewReferences();
		loadPreferences();
		
		// enable app icon as button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		// open the database
		adapter = new DbAdapter(this);
		adapter.open();
	}
	
	/**
	 * Gets references to Views so they can be accessed programmatically
	 */
	private void getViewReferences() {
		titleEditText = (EditText)findViewById(R.id.titleEditText);
		noteEditText = (EditText)findViewById(R.id.noteEditText);
	}
	
	/**
	 * Applies user's preferences to the layout
	 */
	private void loadPreferences() {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		
		noteEditText.setTextSize(Float.parseFloat(preferences.getString("text_size", null)));
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.create_note, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			if (!emptyNote()) saveNote();
			finish();
			return true;
		case R.id.save_note:
			saveNote();
			finish();
			return true;
		case R.id.cancel_changes:
			cancelNote();
			return true;
		case R.id.tags:
			showTags();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * Adds the note to the database
	 */
	private void saveNote() {
		String title = titleEditText.getText().toString();
		String note = noteEditText.getText().toString();
		
		if (title.trim().equals("")) title = "Untitled Note";
		
		if (noteId != -1) adapter.updateNoteSilent(noteId, title, note);
		else noteId = adapter.createNote(title, note);
		noteSaved = true;
	}
	
	/**
	 * Displays the tags dialog
	 */
	private void showTags() {
		// save note before launching TagsActivity (the note must have an id
		// in order for tags to be added to it)
		saveNote();
		Intent intent = new Intent(this, TagsActivity.class);
		intent.putExtra("id", noteId);
		startActivityForResult(intent, SET_TAGS);
	}
	
	/**
	 * Flags the note to be discarded, deletes it from the database if it has already
	 * been added (e.g., if the tags dialog was opened), and finishes the activity
	 */
	private void cancelNote() {
		discardNote = true;
		if (noteSaved) adapter.deleteNote(noteId);
		finish();
	}
	
	/**
	 * Returns true if both the title and note are empty; false otherwise
	 */
	private boolean emptyNote() {
		String title = titleEditText.getText().toString();
		String note = noteEditText.getText().toString();
		
		if (title.equals("") || note.equals(""))
			return true;
		else return false;
	}
	
	@Override
	protected void onPause() {
		// manually hide the soft keyboard - Android closes it automatically, but only after
		// the activity is destroyed (which causes a slight delay in the interface)
		((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(noteEditText.getWindowToken(), 0);
		
		super.onPause();
	}
	
	@Override
	protected void onDestroy() {
		// save the note unless the user has hit cancel
		if (!discardNote) saveNote();
		super.onDestroy();
		if (adapter != null) adapter.close();
	}
	
	@Override
	public void onBackPressed() {
		// if the note is not empty, prompt to save it before closing
		if (!emptyNote()) {
			new AlertDialog.Builder(this)
				.setTitle("Discard Note")
				.setMessage("Would you like to save this note before exiting?")
				.setPositiveButton("Save", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						saveNote();
						finish();
					}
				})
				.setNegativeButton("Discard", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						cancelNote();
					}
				})
				.show();
		}
		else cancelNote();
	}
}