package com.mturco.notes;

import java.util.ArrayList;
import java.util.Date;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class DbAdapter {
	
	private Context context;
	private SQLiteDatabase db;
	private DbHelper helper;
	
	public DbAdapter(Context context) {
		this.context = context;
	}
	
	public DbAdapter open() throws SQLException {
		helper = new DbHelper(context);
		db = helper.getWritableDatabase();
		return this;
	}
	
	public void close() {
		helper.close();
	}
	
	public long createNote(String title, String note) {
		ContentValues values = new ContentValues();
		long now = (new Date()).getTime();
		values.put(DbHelper.NOTES_TITLE_FIELD, title);
		values.put(DbHelper.NOTES_NOTE_FIELD, note);
		values.put(DbHelper.NOTES_DATE_CREATED_FIELD, now);
		values.put(DbHelper.NOTES_DATE_MODIFIED_FIELD, now);
		return db.insert(DbHelper.NOTES_TABLE, null, values);
	}
	
	public boolean renameNote(long id, String title) {
		ContentValues values = new ContentValues();
		values.put(DbHelper.NOTES_ID_FIELD, id);
		values.put(DbHelper.NOTES_TITLE_FIELD, title);
		return db.update(DbHelper.NOTES_TABLE, values, DbHelper.NOTES_ID_FIELD + "=" + id, null) > 0;
	}
	
	public boolean updateNote(long id, String title, String note) {
		ContentValues values = new ContentValues();
		values.put(DbHelper.NOTES_ID_FIELD, id);
		values.put(DbHelper.NOTES_TITLE_FIELD, title);
		values.put(DbHelper.NOTES_NOTE_FIELD, note);
		values.put(DbHelper.NOTES_DATE_MODIFIED_FIELD, (new Date()).getTime());
		return db.update(DbHelper.NOTES_TABLE, values, DbHelper.NOTES_ID_FIELD + "=" + id, null) > 0;
	}
	
	public boolean updateNoteSilent(long id, String title, String note) {
		ContentValues values = new ContentValues();
		values.put(DbHelper.NOTES_ID_FIELD, id);
		values.put(DbHelper.NOTES_TITLE_FIELD, title);
		values.put(DbHelper.NOTES_NOTE_FIELD, note);
		return db.update(DbHelper.NOTES_TABLE, values, DbHelper.NOTES_ID_FIELD + "=" + id, null) > 0;
	}
	
	public boolean deleteNote(long id) {
		removeAllTags(id);
		return db.delete(DbHelper.NOTES_TABLE, DbHelper.NOTES_ID_FIELD + "=" + id, null) > 0;
	}
	
	public boolean deleteAllNotes() {
		clearTags();
		return db.delete(DbHelper.NOTES_TABLE, null, null) > 0;
	}
	
	public Cursor getAllNotes() {
		return db.query(DbHelper.NOTES_TABLE, new String[] {
				DbHelper.NOTES_ID_FIELD,
				DbHelper.NOTES_TITLE_FIELD,
				DbHelper.NOTES_NOTE_FIELD,
				DbHelper.NOTES_DATE_CREATED_FIELD,
				DbHelper.NOTES_DATE_MODIFIED_FIELD
			}, null, null, null, null, null);
	}
	
	public Cursor getAllNotes(String orderBy) {
		return db.query(DbHelper.NOTES_TABLE, new String[] {
				DbHelper.NOTES_ID_FIELD,
				DbHelper.NOTES_TITLE_FIELD,
				DbHelper.NOTES_NOTE_FIELD,
				DbHelper.NOTES_DATE_CREATED_FIELD,
				DbHelper.NOTES_DATE_MODIFIED_FIELD
			}, null, null, null, null, orderBy);
	}
	
	public Cursor getNotes(String where, String orderBy) {
		return db.query(DbHelper.NOTES_TABLE, new String[] {
				DbHelper.NOTES_ID_FIELD,
				DbHelper.NOTES_TITLE_FIELD,
				DbHelper.NOTES_NOTE_FIELD,
				DbHelper.NOTES_DATE_CREATED_FIELD,
				DbHelper.NOTES_DATE_MODIFIED_FIELD
			}, where, null, null, null, orderBy);
	}
	
	public Cursor getNote(long id) {
		Cursor cursor = db.query(DbHelper.NOTES_TABLE, new String[] {
				DbHelper.NOTES_ID_FIELD,
				DbHelper.NOTES_TITLE_FIELD,
				DbHelper.NOTES_NOTE_FIELD,
				DbHelper.NOTES_DATE_CREATED_FIELD,
				DbHelper.NOTES_DATE_MODIFIED_FIELD
			}, DbHelper.NOTES_ID_FIELD + "=" + id, null, null, null, null);
		if (cursor != null) cursor.moveToFirst();
		return cursor;
	}
	
	public ArrayList<String> getDistinctTags() {
		Cursor cursor = db.rawQuery(
				"select distinct " + DbHelper.TAGS_TAG_FIELD + " " +
				"from " + DbHelper.TAGS_TABLE
				, null);
		
		ArrayList<String> tags = new ArrayList<String>();
		while (cursor.moveToNext())
			tags.add(cursor.getString(cursor.getColumnIndex(DbHelper.TAGS_TAG_FIELD)));
		
		return tags;
	}
	
	public Cursor getTags(long noteId) {
		return db.query(DbHelper.TAGS_TABLE, new String[] {
				DbHelper.TAGS_ID_FIELD,
				DbHelper.TAGS_TAG_FIELD
				}, DbHelper.TAGS_NOTE_ID_FIELD + "=" + noteId, null, null, null, "tag asc");
	}
	
	public long addTag(long noteId, String tag) {
		ContentValues values = new ContentValues();
		values.put(DbHelper.TAGS_NOTE_ID_FIELD, noteId);
		values.put(DbHelper.TAGS_TAG_FIELD, tag);
		return db.insert(DbHelper.TAGS_TABLE, null, values);
	}
	
	public boolean removeTag(long noteId, long tagId) {
		return db.delete(DbHelper.TAGS_TABLE,
				DbHelper.TAGS_NOTE_ID_FIELD + "=? AND " + DbHelper.TAGS_ID_FIELD + "=?",
				new String[] { String.valueOf(noteId), String.valueOf(tagId) }) > 0;
	}
	
	public boolean removeTag(long noteId, String tag) {
		return db.delete(DbHelper.TAGS_TABLE,
				DbHelper.TAGS_NOTE_ID_FIELD + "=? AND " + DbHelper.TAGS_TAG_FIELD + "=?",
				new String[] { String.valueOf(noteId), tag }) > 0;
	}
	
	public boolean removeAllTags(long noteId) {
		return db.delete(DbHelper.TAGS_TABLE,
				DbHelper.TAGS_NOTE_ID_FIELD + "=" + noteId, null) > 0;
	}
	
	public boolean deleteTag(String tag) {
		return db.delete(DbHelper.TAGS_TABLE,
				DbHelper.TAGS_TAG_FIELD + "=" + tag, null) > 0;
	}
	
	public boolean clearTags() {
		return db.delete(DbHelper.TAGS_TABLE, null, null) > 0;
	}
}