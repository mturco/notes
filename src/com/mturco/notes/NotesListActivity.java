package com.mturco.notes;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.TextView;

/**
 * (Main Activity) Lists all of the notes.
 */
public class NotesListActivity extends ListActivity {
	private static final int VIEW_NOTE = 1;
	private static final int EDIT_NOTE = 2;
	private static final int CREATE_NOTE = 3;
	private static final int SET_TAGS = 4;
	private static final int VIEW_DETAILS = 5;
	private static final int PREFERENCES = 6;
	private DbAdapter adapter;
	private Cursor cursor;
	private SharedPreferences preferences;
	private OnSharedPreferenceChangeListener preferencesListener;
	private String sortOrder;
	private boolean restartRequired = false;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		// set the default preferences
		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
		loadPreferences();
		// register a listener for when a preference changes
		preferencesListener = new OnSharedPreferenceChangeListener() {
			public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
				if (key.equals("app_theme")) restartRequired = true;
				loadPreferences();
				loadNotes();
			}
		};
		preferences.registerOnSharedPreferenceChangeListener(preferencesListener);
		
		setContentView(R.layout.notes_list);
		ListView listView = getListView();
		listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
		listView.setMultiChoiceModeListener(new ActionModeCallback());
		
		// open the database and load the notes
		adapter = new DbAdapter(this);
		adapter.open();
		loadNotes();
	}
	
	/**
	 * Applies user's preferences to the layout
	 */
	private void loadPreferences() {
		preferences = PreferenceManager.getDefaultSharedPreferences(this);
		
		if (preferences.getString("app_theme", null).equals("dark"))
			setTheme(android.R.style.Theme_Holo);
		else setTheme(android.R.style.Theme_Holo_Light);
		sortOrder = preferences.getString("sort_order", null);
	}
	
	/**
	 * Loads the notes into the ListView
	 */
	public void loadNotes() {
		cursor = adapter.getAllNotes(sortOrder);
		startManagingCursor(cursor);
		
		((TextView)findViewById(android.R.id.empty)).setText(R.string.no_notes);
		
		String[] from = new String[] { DbHelper.NOTES_TITLE_FIELD,
			DbHelper.NOTES_DATE_MODIFIED_FIELD };
		int[] to = new int[] { android.R.id.text1, android.R.id.text2 };
		setListAdapter(new NotesCursorAdapter(this, R.layout.note_list_item, cursor,
			from, to));
	}
	
	/**
	 * Loads the notes into the ListView
	 */
	public void loadNotes(String query) {
		cursor = adapter.getNotes(DbHelper.NOTES_TITLE_FIELD + " like '%" + query
			+ "%'", sortOrder);
		startManagingCursor(cursor);
		
		((TextView)findViewById(android.R.id.empty)).setText(R.string.no_notes_found);
		
		String[] from = new String[] { DbHelper.NOTES_TITLE_FIELD,
			DbHelper.NOTES_DATE_MODIFIED_FIELD };
		int[] to = new int[] { android.R.id.text1, android.R.id.text2 };
		setListAdapter(new NotesCursorAdapter(this, R.layout.note_list_item, cursor,
			from, to));
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.notes_list, menu);
		SearchView searchView = (SearchView)menu.findItem(R.id.search).getActionView();
		searchView.setOnQueryTextListener(new OnQueryTextListener() {
			@Override
			public boolean onQueryTextChange(String newText) {
				loadNotes(newText);
				return true;
			}
			
			@Override
			public boolean onQueryTextSubmit(String query) {
				return true;
			}
		});
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.new_note:
			createNote();
			return true;
		case R.id.preferences:
			// launch PreferencesActivity
			Intent intent = new Intent(this, PreferencesActivity.class);
			startActivityForResult(intent, PREFERENCES);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		// launch ViewNoteActivity
		Intent intent = new Intent(this, NoteActivity.class);
		intent.putExtra("id", id);
		startActivityForResult(intent, VIEW_NOTE);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
		super.onActivityResult(requestCode, resultCode, intent);
		switch (requestCode) {
		case CREATE_NOTE:
		case EDIT_NOTE:
			// reload notes if a note was added or modified
			loadNotes();
			break;
		case PREFERENCES:
			if (restartRequired)
				new AlertDialog.Builder(this)
					.setTitle("Change Theme")
					.setMessage(
						getResources().getString(R.string.app_name)
							+ " needs to restart in order to switch themes. Would you like to restart now?")
					.setPositiveButton("Restart", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent intent = new Intent(getBaseContext(),
								NotesListActivity.class);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							startActivity(intent);
						}
					}).setNegativeButton("Later", null).show();
			break;
		}
	}
	
	/**
	 * Launches CreateNoteActivity
	 */
	private void createNote() {
		Intent intent = new Intent(this, CreateNoteActivity.class);
		startActivityForResult(intent, CREATE_NOTE);
	}
	
	/**
	 * Launches EditNoteActivity
	 */
	private void editNote(long id) {
		Intent intent = new Intent(this, EditNoteActivity.class);
		intent.putExtra("id", id);
		startActivityForResult(intent, EDIT_NOTE);
	}
	
	/**
	 * Prompts the user for a new title, then renames the note
	 */
	private void renameNote(final long id) {
		LayoutInflater inflater = LayoutInflater.from(this);
		View layout = inflater.inflate(R.layout.rename_note,
			(ViewGroup)findViewById(R.id.layoutRoot));
		final EditText titleEditText = (EditText)layout.findViewById(R.id.titleEditText);
		
		// load the current title into the EditText
		Cursor note = adapter.getNote(id);
		titleEditText.setText(note.getString(note
			.getColumnIndex(DbHelper.NOTES_TITLE_FIELD)));
		// put the cursor at the end of the EditText
		titleEditText.setSelection(titleEditText.getText().length());
		
		AlertDialog renameDialog = new AlertDialog.Builder(this).setTitle("Rename Note")
			.setView(layout)
			.setPositiveButton("Rename", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					adapter.renameNote(id, titleEditText.getText().toString());
					loadNotes();
				}
			}).setNegativeButton("Cancel", null).show();
		renameDialog.getWindow().setSoftInputMode(
			WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
	}
	
	/**
	 * Launches TagsActivity
	 */
	private void showTags(long id) {
		Intent intent = new Intent(this, TagsActivity.class);
		intent.putExtra("id", id);
		startActivityForResult(intent, SET_TAGS);
	}
	
	/**
	 * Launches ViewDetailsActivity
	 */
	private void showDetails(long id) {
		Intent intent = new Intent(this, DetailsActivity.class);
		intent.putExtra("id", id);
		startActivityForResult(intent, VIEW_DETAILS);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (adapter != null) adapter.close();
	}
	
	/**
	 * Handles all everything involving the "action mode" (or selection mode) which is
	 * launched when a user long presses on a list item.
	 */
	private class ActionModeCallback implements ListView.MultiChoiceModeListener {
		private MenuItem editMenuItem;
		private MenuItem renameMenuItem;
		private MenuItem tagsMenuItem;
		private MenuItem detailsMenuItem;
		
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.notes_list_selection, menu);
			editMenuItem = menu.findItem(R.id.edit_note);
			renameMenuItem = menu.findItem(R.id.rename_note);
			tagsMenuItem = menu.findItem(R.id.tags);
			detailsMenuItem = menu.findItem(R.id.details);
			
			mode.setTitle("Select notes");
			return true;
		}
		
		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return true;
		}
		
		@Override
		public boolean onActionItemClicked(final ActionMode mode, MenuItem item) {
			final long[] itemIds = getListView().getCheckedItemIds();
			
			switch (item.getItemId()) {
			case R.id.delete_note:
				// display a confirmation dialog before deleting
				new AlertDialog.Builder(NotesListActivity.this)
					.setTitle("Delete Selected Notes")
					.setMessage("Are you sure you want to delete the selected notes?")
					.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							for (long id : itemIds)
								adapter.deleteNote(id);
							loadNotes();
							mode.finish();
						}
					}).setNegativeButton("Cancel", null).show();
				break;
			case R.id.edit_note:
				editNote(itemIds[0]);
				break;
			case R.id.rename_note:
				renameNote(itemIds[0]);
				mode.finish();
				break;
			case R.id.tags:
				showTags(itemIds[0]);
				mode.finish();
				break;
			case R.id.details:
				showDetails(itemIds[0]);
				mode.finish();
				return true;
			}
			return true;
		}
		
		@Override
		public void onDestroyActionMode(ActionMode mode) {
		}
		
		@Override
		public void onItemCheckedStateChanged(ActionMode mode, int position, long id,
			boolean checked) {
			int checkedCount = getListView().getCheckedItemCount();
			switch (checkedCount) {
			case 0:
				mode.setSubtitle("Nothing selected");
				break;
			case 1:
				mode.setSubtitle(checkedCount + " selected");
				// display additional menu items if only one note is selected
				editMenuItem.setVisible(true);
				renameMenuItem.setVisible(true);
				tagsMenuItem.setVisible(true);
				detailsMenuItem.setVisible(true);
				break;
			default:
				mode.setSubtitle(checkedCount + " selected");
				// hide menu items that cannot be used on multiple notes simultaneously
				editMenuItem.setVisible(false);
				renameMenuItem.setVisible(false);
				tagsMenuItem.setVisible(false);
				detailsMenuItem.setVisible(false);
				break;
			}
		}
	}
}