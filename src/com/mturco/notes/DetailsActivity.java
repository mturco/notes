package com.mturco.notes;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class DetailsActivity extends Activity {
	private DbAdapter adapter;
	private long noteId;
	
	private TextView dateCreatedTextView;
	private TextView dateModifiedTextView;
	private TextView wordCountTextView;
	private TextView characterCountTextView;
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.details);
		getViewReferences();
		
		// retrieve the note's id from the extras (failing that, finish the activity)
		if (getIntent().getExtras() != null)
			noteId = getIntent().getExtras().getLong("id");
		else finish();

		// open the database and load the note
		adapter = new DbAdapter(this);
		adapter.open();
		loadNote();
	}
	
	private void getViewReferences() {
		dateCreatedTextView = (TextView)findViewById(R.id.dateCreatedTextView);
		dateModifiedTextView = (TextView)findViewById(R.id.dateModifiedTextView);
		wordCountTextView = (TextView)findViewById(R.id.wordCountTextView);
		characterCountTextView = (TextView)findViewById(R.id.characterCountTextView);
	}

	/**
	 * Loads the note from the database into the appropriate Views
	 */
	private void loadNote() {
		Cursor cursor = adapter.getNote(noteId);
		startManagingCursor(cursor);
		
		String title = cursor.getString(cursor.getColumnIndex(DbHelper.NOTES_TITLE_FIELD));
		String note = cursor.getString(cursor.getColumnIndex(DbHelper.NOTES_NOTE_FIELD));
		long dateCreated = cursor.getLong(cursor.getColumnIndex(DbHelper.NOTES_DATE_CREATED_FIELD));
		long dateModified = cursor.getLong(cursor.getColumnIndex(DbHelper.NOTES_DATE_MODIFIED_FIELD));
		int characterCount = note.length();
		int wordCount = (characterCount > 0 ? note.split("[^0-9a-zA-Z']+").length : 0);
		
		setTitle(title);
		dateCreatedTextView.setText(formatDate(new Date(dateCreated)));
		Log.d("dates", "c:"+dateCreated + "|m:"+dateModified);
		dateModifiedTextView.setText(dateModified == dateCreated ? "Never" : formatDate(new Date(dateModified)));
		wordCountTextView.setText(String.valueOf(wordCount));
		characterCountTextView.setText(String.valueOf(characterCount));
	}
	
	public static String formatDate(Date date) {
		SimpleDateFormat sdf;
		String stringDate;
		
		Calendar dateToFormat = Calendar.getInstance();
		dateToFormat.setTime(date);
		
		Calendar today = Calendar.getInstance();
		today.set(Calendar.HOUR_OF_DAY, 0);
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.SECOND, 0);
		today.set(Calendar.MILLISECOND, 0);

		Calendar yesterday = (Calendar)today.clone();
		yesterday.add(Calendar.DAY_OF_YEAR, -1);
		
		Calendar thisYear = (Calendar)today.clone();
		thisYear.set(Calendar.DAY_OF_YEAR, 1);
		
		// is it sometime today?
		if (dateToFormat.after(today)) {
			sdf = new SimpleDateFormat("'Today at' h:mma");
			stringDate = sdf.format(date);
		}
		// is it sometime yesterday?
		else if (dateToFormat.after(yesterday)) {
			sdf = new SimpleDateFormat("'Yesterday at' h:mma");
			stringDate = sdf.format(date);
		}
		// is it sometime this year?
		else if (dateToFormat.after(thisYear)) {
			sdf = new SimpleDateFormat("MMM d 'at' h:mma");
			stringDate = sdf.format(date);
		}
		// it wasn't this year, so show the year
		else {
			sdf = new SimpleDateFormat("MMM d, y 'at' h:mma");
			stringDate = sdf.format(date);
		}
		
		return stringDate;
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (adapter != null) adapter.close();
	}
}