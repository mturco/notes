package com.mturco.notes;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * Used to edit existing notes.
 * 
 * Note: This activity is nearly identical to CreateNoteActivity (which is used for
 * creating new notes). The only visual difference is the absence of the title EditText
 * (omitted to save screen real estate while editing).
 * 
 * @see CreateNoteActivity
 */
public class EditNoteActivity extends Activity {
	private static final int SET_TAGS = 1;
	private DbAdapter adapter;
	private long noteId;
	private boolean discardChanges = false;
	private EditText noteEditText;
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.edit_note);
		getViewReferences();
		loadPreferences();

		// enable app icon as button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		// retrieve the note's id from the extras (failing that, finish the activity)
		if (getIntent().getExtras() != null)
			noteId = getIntent().getExtras().getLong("id");
		else finish();
		
		// open the database and load the note
		adapter = new DbAdapter(this);
		adapter.open();
		loadNote();
	}
	
	/**
	 * Gets references to Views so they can be accessed programmatically
	 */
	private void getViewReferences() {
		noteEditText = (EditText)findViewById(R.id.noteEditText);
	}
	
	/**
	 * Applies user's preferences to the layout
	 */
	private void loadPreferences() {
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		
		noteEditText.setTextSize(Float.parseFloat(preferences.getString("text_size", null)));
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.edit_note, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			saveNote();
			finish();
			return true;
		case R.id.save_note:
			saveNote();
			finish();
			return true;
		case R.id.cancel_changes:
			cancelChanges();
			return true;
		case R.id.tags:
			showTags();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * Loads the note from the database into the appropriate Views
	 */
	private void loadNote() {
		Cursor cursor = adapter.getNote(noteId);
		startManagingCursor(cursor);
		
		setTitle(cursor.getString(cursor.getColumnIndex(DbHelper.NOTES_TITLE_FIELD)));
		noteEditText.setText(cursor.getString(cursor.getColumnIndex(DbHelper.NOTES_NOTE_FIELD)));
	}
	
	/**
	 * Update the note in the database
	 */
	private void saveNote() {
		String title = getTitle().toString();
		String note = noteEditText.getText().toString();
		adapter.updateNote(noteId, title, note);
	}

	/**
	 * Displays the tags dialog
	 */
	private void showTags() {
		Intent intent = new Intent(this, TagsActivity.class);
		intent.putExtra("id", noteId);
		startActivityForResult(intent, SET_TAGS);
	}
	
	/**
	 * Flags the changes to be discarded and finishes the activity
	 */
	private void cancelChanges() {
		discardChanges = true;
		finish();
	}
	
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable("id", noteId);
	}
	
	@Override
	protected void onPause() {
		// manually hide the soft keyboard - Android closes it automatically, but only after
		// the activity is destroyed (which causes a slight delay in the interface)
		((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(noteEditText.getWindowToken(), 0);
		
		super.onPause();
	}
	
	@Override
	protected void onDestroy() {
		// save the note unless the user has hit cancel
		if (!discardChanges) saveNote();
		super.onDestroy();
		if (adapter != null) adapter.close();
	}
	
	@Override
	public void onBackPressed() {
		// load the version of the note currently stored in the database
		Cursor cursor = adapter.getNote(noteId);
		String savedNote = cursor.getString(cursor.getColumnIndex(DbHelper.NOTES_NOTE_FIELD));
		cursor.close();
		
		// compare it to the current note and prompt to save changes if they are not the same
		if (noteEditText.getText().toString().compareTo(savedNote) != 0) {
			new AlertDialog.Builder(this)
				.setTitle("Discard Changes")
				.setMessage("Would you like to save this note before exiting?")
				.setPositiveButton("Save", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						saveNote();
						finish();
					}
				})
				.setNegativeButton("Discard", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						cancelChanges();
					}
				})
				.show();
		}
		else finish();
	}
}