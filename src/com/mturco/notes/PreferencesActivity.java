package com.mturco.notes;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

public class PreferencesActivity extends PreferenceActivity {
	private DbAdapter adapter;
	private Preference deleteAllNotesButton;
	private Preference backupNotesButton;
	private Preference restoreNotesButton;
	private Preference deleteBackupButton;
	private Context context;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
		getViewReferences();
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		context = this;
		
		adapter = new DbAdapter(this);
		adapter.open();
		
		deleteAllNotesButton.setEnabled(adapter.getAllNotes().getCount() > 0);
		deleteAllNotesButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				// display a confirmation dialog before deleting everything
				new AlertDialog.Builder(context)
				.setTitle("Delete All Notes")
				.setMessage("Are you sure you want to delete all of your notes?")
				.setPositiveButton("Delete All", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						adapter.deleteAllNotes();
						deleteAllNotesButton.setEnabled(false);
					}
				})
				.setNegativeButton("Cancel", null)
				.show();
				return true;
			}
		});
		
		checkForBackups();
		
		backupNotesButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
				View layout = inflater.inflate(R.layout.backup_notes, (ViewGroup)findViewById(R.id.layoutRoot));
				final EditText backupNameEditText = (EditText)layout.findViewById(R.id.backupNameEditText);
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_kkmmss");
				String suggestedName = "backup_" + sdf.format(new Date());
				backupNameEditText.setText(suggestedName);
				backupNameEditText.setSelection(0, suggestedName.length());
				
				// only allow numbers, letters, underscores, and hyphens to be typed
				InputFilter backupNameFilter = new InputFilter() {
					@Override
					public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
						char c;
						for (int i = start; i < end; i++) {
							c = source.charAt(i);
							if (!Character.isLetterOrDigit(c) && c != '_' && c != '-')
								return "";
						}
						return null;
					}
				};
				backupNameEditText.setFilters(new InputFilter[] { backupNameFilter });
				
				AlertDialog backupNotesDialog = new AlertDialog.Builder(context)
					.setTitle("Backup Notes")
					.setView(layout)
					.setPositiveButton("Backup Notes", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// hide soft keyboard first
							((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(backupNameEditText.getWindowToken(), 0);
							backupNotes(backupNameEditText.getText().toString());
						}
					})
					.setNegativeButton("Cancel", null)
					.create();
				
				backupNotesDialog.show();
				backupNotesDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
				
				final Button backupButton = backupNotesDialog.getButton(AlertDialog.BUTTON_POSITIVE);
				
				backupNameEditText.addTextChangedListener(new TextWatcher() {
					@Override
					public void afterTextChanged(Editable s) { }
					@Override
					public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
					@Override
					public void onTextChanged(CharSequence s, int start, int before, int count) {
						backupButton.setEnabled(backupNameEditText.getText().length() > 0);
					}
				});
				return true;
			}
		});
		
		restoreNotesButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
		        File sd = Environment.getExternalStorageDirectory();
	            File backupDir = new File(sd.getAbsolutePath() + "/.notes/backup");
	            
	            File[] backupFiles = sortByDateModified(backupDir.listFiles());
	            final ArrayList<String> backups = new ArrayList<String>();
	            for (File file : backupFiles) backups.add(file.getName());
	            
				new AlertDialog.Builder(context)
					.setTitle("Restore Backup")
					.setItems(backups.toArray(new String[backups.size()]), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							restoreBackup(backups.get(which));
						}
					})
					.show();
		        
				return true;
			}
		});
		
		deleteBackupButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
		        File sd = Environment.getExternalStorageDirectory();
	            File backupDir = new File(sd.getAbsolutePath() + "/.notes/backup");
	            
	            File[] backupFiles = sortByDateModified(backupDir.listFiles());
	            final ArrayList<String> backups = new ArrayList<String>();
	            for (File file : backupFiles) backups.add(file.getName());
	            
				new AlertDialog.Builder(context)
					.setTitle("Delete Backup")
					.setItems(backups.toArray(new String[backups.size()]), new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							final String filename = backups.get(which);
							new AlertDialog.Builder(context)
								.setTitle("Delete Backup")
								.setMessage("Are you sure you want to delete this backup?")
								.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										deleteBackup(filename);
									}
								})
								.setNegativeButton("Cancel", null)
								.show();
						}
					})
					.show();
		        
				return true;
			}
		});
	}
	
	private void backupNotes(final String filename) {
        final File backup = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.notes/backup/" + filename);
        
        if (backup.exists()) {
        	new AlertDialog.Builder(context)
            	.setTitle("Overwrite Backup")
            	.setMessage("A backup with this name already exists. Do you want to overwrite it?")
            	.setPositiveButton("Overwrite", new DialogInterface.OnClickListener() {
        			@Override
        			public void onClick(DialogInterface dialog, int which) {
        				createBackup(backup);
        			}
                })
            	.setNegativeButton("Cancel", null)
            	.show();
        }
        else createBackup(backup);
	}
	
	private void createBackup(File backup) {
		File appDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.notes");
		File backupDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.notes/backup");
        if (!appDir.exists() || !appDir.isDirectory()) appDir.mkdir();
        if (!backupDir.exists() || !backupDir.isDirectory()) backupDir.mkdir();
        
        File currentDb = getDatabasePath(DbHelper.DATABASE_NAME);
        
		if (currentDb.exists()) {
			try {
                FileChannel source = new FileInputStream(currentDb).getChannel();
                FileChannel destination = new FileOutputStream(backup).getChannel();
                destination.transferFrom(source, 0, source.size());
                source.close();
                destination.close();
			}
            catch (Exception e) {
            	Log.e("PreferencesActivity", "Exception: " + e.getMessage());
            	new AlertDialog.Builder(context)
    	        	.setTitle("Backup Error")
    	        	.setMessage("There was an error creating the backup.")
    	        	.setNeutralButton("Ok", null)
    	        	.show();
            }
        }
        
        if (backup.exists()) {
        	new AlertDialog.Builder(context)
            	.setTitle("Backup Created")
            	.setMessage("A backup of your notes has been created.")
            	.setNeutralButton("Ok", null)
            	.show();
            	checkForBackups();
        }
	}
	
	private void restoreBackup(String filename) {
        File backupDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.notes/backup");
        File backup = new File(backupDir, filename);
        
		if (backup.exists()) {
	        if (adapter != null) adapter.close();
	        
	        DbHelper helper = new DbHelper(context);
	        if (helper.importDatabase(backup.getAbsolutePath())) {
            	new AlertDialog.Builder(context)
	            	.setTitle("Backup Restored")
	            	.setMessage("Your notes have been restored.")
	            	.setNeutralButton("Ok", null)
	            	.show();
	        }
	        adapter.open();
			deleteAllNotesButton.setEnabled(adapter.getAllNotes().getCount() > 0);
		}
	}
	
	private void deleteBackup(String filename) {
		File backupDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/.notes/backup");
        File backup = new File(backupDir, filename);
        if (backup.exists()) backup.delete();
        checkForBackups();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void getViewReferences() {
		deleteAllNotesButton = (Preference)findPreference("delete_all_notes");
		backupNotesButton = (Preference)findPreference("backup_notes");
		restoreNotesButton = (Preference)findPreference("restore_notes");
		deleteBackupButton = (Preference)findPreference("delete_backup");
	}
	
	private boolean checkForBackups() {
		File backupDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
				+ "/.notes/backup");
		
		File[] backupFiles = (backupDir.exists() ? backupDir.listFiles() : null);
		if (backupFiles != null && backupFiles.length > 0) {
			restoreNotesButton.setEnabled(true);
			restoreNotesButton.setSummary(R.string.restore_notes_summary);
			deleteBackupButton.setEnabled(true);
			deleteBackupButton.setSummary(R.string.delete_backup_summary);
			return true;
		}
		else {
			restoreNotesButton.setEnabled(false);
			restoreNotesButton.setSummary("No backup to restore");
			deleteBackupButton.setEnabled(false);
			deleteBackupButton.setSummary("No backup to delete");
			return false;
		}
	}
	
	private File[] sortByDateModified(File[] files) {
		File[] sorted = files;
        File temp;

        for (int x = 1; x < sorted.length; x++) {
            for (int y = 0; y < sorted.length - x; y++) {
                if (sorted[y].lastModified() < sorted[y+1].lastModified()) {
                    temp = sorted[y];
                    sorted[y] = sorted[y+1];
                    sorted[y+1] = temp;
                }
            }
        }
        
        return sorted;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		checkForBackups();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (adapter != null) adapter.close();
	}
}