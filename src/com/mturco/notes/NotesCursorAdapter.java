package com.mturco.notes;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class NotesCursorAdapter extends SimpleCursorAdapter {
	private int layout;
	private TextView text1;
	private TextView text2;
	
	public NotesCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
		super(context, layout, c, from, to);
		this.layout = layout;
	}
	
	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		Cursor c = getCursor();
		LayoutInflater inflater = LayoutInflater.from(context);
		View v = inflater.inflate(layout, parent, false);
		
		String title = c.getString(c.getColumnIndex(DbHelper.NOTES_TITLE_FIELD));
		Date dateModified = new Date(c.getLong(c.getColumnIndex(DbHelper.NOTES_DATE_MODIFIED_FIELD)));
		
		text1 = (TextView)v.findViewById(android.R.id.text1);
		text2 = (TextView)v.findViewById(android.R.id.text2);
		
		text1.setText(title);
		text2.setText(formatDate(dateModified));
		
		return v;
	}
	
	@Override
	public void bindView(View v, Context context, Cursor c) {
		String title = c.getString(c.getColumnIndex(DbHelper.NOTES_TITLE_FIELD));
		Date dateModified = new Date(c.getLong(c.getColumnIndex(DbHelper.NOTES_DATE_MODIFIED_FIELD)));
				
		text1 = (TextView)v.findViewById(android.R.id.text1);
		text2 = (TextView)v.findViewById(android.R.id.text2);
		
		text1.setText(title);
		text2.setText(formatDate(dateModified));
	}
	
	public static String formatDate(Date date) {
		SimpleDateFormat sdf;
		String stringDate;
		
		Calendar dateToFormat = Calendar.getInstance();
		dateToFormat.setTime(date);
		
		Calendar today = Calendar.getInstance();
		today.set(Calendar.HOUR_OF_DAY, 0);
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.SECOND, 0);
		today.set(Calendar.MILLISECOND, 0);
		
		Calendar thisYear = (Calendar)today.clone();
		thisYear.set(Calendar.DAY_OF_YEAR, 1);
		
		// is it sometime today? (ex: 2:35pm)
		if (dateToFormat.after(today)) {
			sdf = new SimpleDateFormat("h:mma");
			stringDate = sdf.format(date).toLowerCase();
		}
		// is it sometime this year? (ex: Jan 5)
		else if (dateToFormat.after(thisYear)) {
			sdf = new SimpleDateFormat("MMM d");
			stringDate = sdf.format(date);
		}
		// it wasn't this year, so show the year (ex: Jan 5, 2011)
		else {
			sdf = new SimpleDateFormat("MMM d, y");
			stringDate = sdf.format(date);
		}
		
		return stringDate;
	}
}