package com.mturco.notes;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Displays a note.
 */
public class NoteActivity extends Activity {
	private static final int ACTIVITY_EDIT = 1;
	private static final int SET_TAGS = 2;
	private static final int ACTIVITY_SETTINGS = 3;
	private DbAdapter adapter;
	private Long noteId;
	private TextView noteTextView;
	private LinearLayout tagsLinearLayout;
	private SharedPreferences preferences;
	private OnSharedPreferenceChangeListener preferencesListener;
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.note);
		getViewReferences();
		loadPreferences();
		
		// register a listener for when a preference changes
		preferencesListener = new OnSharedPreferenceChangeListener() {
			public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
				loadPreferences();
			}
		};
		preferences.registerOnSharedPreferenceChangeListener(preferencesListener);

		// enable app icon as button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		// retrieve the note's id from the extras (failing that, finish the activity)
		if (getIntent().getExtras() != null)
			noteId = getIntent().getExtras().getLong("id");
		else finish();
		
		// open the database and load the note
		adapter = new DbAdapter(this);
		adapter.open();
		loadNote();
	}
	
	/**
	 * Gets references to Views so they can be accessed programmatically
	 */
	private void getViewReferences() {
		noteTextView = (TextView)findViewById(R.id.noteTextView);
		tagsLinearLayout = (LinearLayout)findViewById(R.id.tagsLinearLayout);
	}

	/**
	 * Applies user's preferences to the layout
	 */
	private void loadPreferences() {
		preferences = PreferenceManager.getDefaultSharedPreferences(this);
		
		noteTextView.setTextSize(Float.parseFloat(preferences.getString("text_size", null)));
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.note, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.edit_note:
			editNote();
			return true;
		case R.id.delete_note:
			deleteNote();
			return true;
		case R.id.rename_note:
			renameNote();
			return true;
		case R.id.tags:
			showTags();
			return true;
		case R.id.preferences:
			// launch PreferencesActivity
			Intent intent = new Intent(this, PreferencesActivity.class);
			startActivityForResult(intent, ACTIVITY_SETTINGS);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	/**
	 * Loads the note from the database into the appropriate Views
	 */
	private void loadNote() {
		Cursor cursor = adapter.getNote(noteId);
		startManagingCursor(cursor);
		
		// set the activity's title to the note title
		setTitle(cursor.getString(cursor.getColumnIndex(DbHelper.NOTES_TITLE_FIELD)));
		// load the actual note into noteTextView
		noteTextView.setText(cursor.getString(cursor.getColumnIndex(DbHelper.NOTES_NOTE_FIELD)));
		
		// load the tags
		cursor = adapter.getTags(noteId);
		LinearLayout tagsContainerLinearLayout = (LinearLayout)findViewById(R.id.tagsContainerLinearLayout);
		TextView tag;
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		params.setMargins(7, 0, 7, 0);
		tagsLinearLayout.removeAllViews();
		
		// if there are tags, load them; otherwise, hide the tags container
		if (cursor.getCount() > 0) {
			tagsContainerLinearLayout.setVisibility(View.VISIBLE);
			while (cursor.moveToNext()) {
				tag = new TextView(this);
				tag.setText(cursor.getString(cursor.getColumnIndex(DbHelper.TAGS_TAG_FIELD)));
				tag.setLayoutParams(params);
				tag.setTextColor(getResources().getColor(R.color.accent_color));
				tagsLinearLayout.addView(tag);
			}
		}
		else tagsContainerLinearLayout.setVisibility(View.GONE);
	}
	
	/**
	 * Launches EditNoteActivity
	 */
	private void editNote() {
		Intent intent = new Intent(this, EditNoteActivity.class);
		intent.putExtra("id", noteId);
		startActivityForResult(intent, ACTIVITY_EDIT);
	}
	
	/**
	 * Prompts the user for confirmation, then deletes the note and finishes the activity
	 */
	private void deleteNote() {
		new AlertDialog.Builder(this)
			.setTitle("Delete Note")
			.setMessage("Are you sure you want to delete this note?")
			.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					adapter.deleteNote(noteId);
					finish();
				}
			})
			.setNegativeButton("Cancel", null)
			.show();
	}
	
	/**
	 * Prompts the user for a new title, then renames the note
	 */
	private void renameNote() {
		LayoutInflater inflater = LayoutInflater.from(getApplicationContext());
		View layout = inflater.inflate(R.layout.rename_note, (ViewGroup)findViewById(R.id.layoutRoot));
		final EditText titleEditText = (EditText)layout.findViewById(R.id.titleEditText);
		
		// load the current title into the EditText
		titleEditText.setText(getTitle());
		// put the cursor at the end of the EditText
		titleEditText.setSelection(titleEditText.getText().length());
		
		AlertDialog renameDialog = new AlertDialog.Builder(this)
			.setTitle("Rename Note")
			.setView(layout)
			.setPositiveButton("Rename", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					adapter.renameNote(noteId, titleEditText.getText().toString());
					setTitle(titleEditText.getText());
				}
			})
			.setNegativeButton("Cancel", null)
			.show();
		renameDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
	}
	
	/**
	 * Launches TagsActivity
	 */
	private void showTags() {
		Intent intent = new Intent(this, TagsActivity.class);
		intent.putExtra("id", noteId);
		startActivityForResult(intent, SET_TAGS);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		loadNote();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (adapter != null) adapter.close();
	}
}