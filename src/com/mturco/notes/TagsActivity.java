package com.mturco.notes;

import java.util.ArrayList;

import android.app.ListActivity;
import android.database.Cursor;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

public class TagsActivity extends ListActivity {
	private DbAdapter adapter;
	private long noteId;
	private Cursor cursor;
	private AutoCompleteTextView newTagEditText;
	
	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.tags);
		registerForContextMenu(getListView());
		
		// retrieve the note's id from the extras (failing that, finish the activity)
		if (getIntent().getExtras() != null)
			noteId = getIntent().getExtras().getLong("id");
		else finish();
		
		newTagEditText = (AutoCompleteTextView)findViewById(R.id.newTagEditText);
		// only allow numbers, letters, underscores, and hyphens to be typed
		InputFilter tagNameFilter = new InputFilter() {
			@Override
			public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
				char c;
				for (int i = start; i < end; i++) {
					c = source.charAt(i);
					if (!Character.isLetterOrDigit(c) && c != '_' && c != '-')
						return "";
				}
				return null;
			}
		};
		newTagEditText.setFilters(new InputFilter[] { tagNameFilter });
		
		Button addTagButton = (Button)findViewById(R.id.addTagButton);
		addTagButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				adapter.addTag(noteId, newTagEditText.getText().toString());
				loadTags();
				newTagEditText.setText("");
			}
		});

		// open the database and load the tags
		adapter = new DbAdapter(this);
		adapter.open();
		loadTags();
		
		// give newTagEditText focus so the user can immediately begin typing
		newTagEditText.requestFocus();
	}

	/**
	 * Loads the tags from the database into the ListView
	 */
	private void loadTags() {
		cursor = adapter.getTags(noteId);
		startManagingCursor(cursor);
		
		String[] from = new String[] { DbHelper.TAGS_TAG_FIELD };
		int[] to = new int[] { android.R.id.text1 };
		
		SimpleCursorAdapter tags = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1, cursor, from, to);
		setListAdapter(tags);
		
		// get a list of all the distinct tags for autocompletion
		ArrayList<String> allTags = adapter.getDistinctTags();
		ArrayAdapter<String> autoCompleteAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_dropdown_item_1line, allTags);
		newTagEditText.setAdapter(autoCompleteAdapter);
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		l.showContextMenuForChild(v);
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.tags_context, menu);
	}
	
	public boolean onContextItemSelected (MenuItem item) {
		AdapterContextMenuInfo menuInfo = (AdapterContextMenuInfo) item.getMenuInfo();
		switch (item.getItemId()) {
		case R.id.delete_tag:
			adapter.removeTag(noteId, menuInfo.id);
			loadTags();
			return true;
		}
		return true;
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (adapter != null) adapter.close();
	}
}